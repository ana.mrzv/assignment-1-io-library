section .data

space_character: db 0xA

section .text
 
; DONE Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall

; DONE Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp byte [rdi+rax], 0 ; проходим символы и проверяем, равны ли они нулю
        je .end
        inc rax ; счетчик длины
        jmp .loop
    .end:
        ret

; DONE Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    call string_length ; в rax длина строки
    mov rsi, rdi ; начало строки
    mov rdx, rax ; длина строки
    mov rax, 1 ; запись
    mov rdi, 1 ; дескриптор
    syscall
    ret

; DONE Принимает код символа и выводит его в stdout
print_char:
    mov rax, 1 ; запись
    mov rdx, 1 ; длина
    push rdi;  т.к. в rdi хранится код символа, нельзя записывать туда дескриптор, сначала сохраняем в стеке
    mov rdi, 1 ; дескриптор
    mov rsi, rsp ; начало строки это указатель стека где лежит код
    syscall
    pop rdi
    ret

; DONE Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, space_character
    jmp print_char

; DONE Выводит беззнаковое 8-байтовое число в десятичном формате 
print_uint: 
    push r12
    push r14
    mov r14, rsp
    mov rax, rdi ; строка для деления должна храниться в rax
    mov r12, 10 ; делитель
    dec rsp
    .loop:
        xor rdx, rdx ; сюда будет записываться остаток от div
        div r12 ; в rax целая часть, в rdx остаток
        add rdx, 48 ; перевод в ascii
        dec rsp
        mov [rsp], dl ; заносим цифру в стек (с конца)
        test rax, rax ; целая часть ноль?
        jnz .loop
    .output:
        mov rdi, rsp
        call print_string ; указали на начало буфера и печатаем слово
    mov rsp, r14
    pop r14
    pop r12
    ret

; DONE Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi ; выставляем флаги
    jns print_uint ; если число отриц, обработка отдельно
    push rdi ; запоминаем указатель
    mov rdi, 45 ; записываем минус
    call print_char ; выводим минус
    pop rdi 
    neg rdi
    jmp print_uint

;  Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    push r12
    push r13
    xor rax, rax ; по умолч вернет 0
    xor rcx, rcx
    .loop:
        mov r13b, byte[rdi+rcx] ; считали символ 1 стрк
        mov r12b, byte[rsi+rcx] ; считали символ 2 стрк
        inc rcx ; +1 к указателю на след. символ
        cmp r13, r12 ; сравниваем символы
        je .check_null ; конец строки?
        pop r13
        pop r12
        ret ; если символы разные вернется 0
    .check_null:
        test r13, r12 ; проверяем на ноль 
        jnz .loop
        inc rax
        pop r13
        pop r12
        ret

; DONE Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax ; чтение
    push rax ; пишем 0 на верх стека чтобы его потом вернуть
    xor rdi, rdi ; дескриптор 0
    mov rdx, 1 ; длина
    mov rsi, rsp ; пишем в стек
    syscall
    pop rax ; возвращаем символ
    ret 

; DONE Принимает: адрес начала буфера rdi, размер буфера rsi
; Читает в буфер слово из stdin, пропуская пробельные символы в начале 0x20, 0x9 и 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    push r12
    push r14
    mov r12, rdi    ; начало
    xor r14, r14    ; для смещения
    .check_spaces:  ; проверка на нули в начале
        call read_char
        cmp rax, 0x20
        je .check_spaces
        cmp rax, 0x9
        je .check_spaces
        cmp rax, 0xA
        je .check_spaces
        cmp rax, 0
        je .write_null
    .loop:
        cmp r14, rsi ; проверяем есть ли еще место в буфере
        je .error
        mov [r12 + r14], al ; записываем символ
        inc r14
        call read_char ; считываем новый и проверяем на конец строки
        cmp rax, 0
        je .write_null
        cmp rax, 0x20
        je .write_null
        cmp rax, 0x9
        je .write_null
        cmp rax, 0xA
        je .write_null
        jmp .loop
    .write_null:
        mov byte [r12 + r14], 0 
        mov rax, r12
        mov rdx, r14
        pop r14
        pop r12
        ret
    .error:
        mov rax, 0
        pop r14
        pop r12
        ret

; DONE Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rdx, rdx ; хранит длину
    xor rax, rax
    xor r8, r8 ; сюда записывается число
    .first:     ; для считывания первой цифры
        mov r8b, byte[rdi]
        cmp r8b, '0'
        jb .no
        cmp r8b, '9'
        ja .no
        sub r8b, '0'
        mov al, r8b
        inc rdx
    .next:      ; для последующих
        mov r8b, byte[rdi+rdx]
        cmp r8b, '0'
        jb .ok
        cmp r8b, '9'
        ja .ok
        inc rdx
        imul rax, 0xA ; rax*10
        sub r8b, '0'
        add rax, r8
        jmp .next
    .no:
        mov rdx, 0
    .ok:
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor rcx, rcx ; длина
    .sign:
        cmp byte[rdi], '-'
        jne parse_uint
    .neg:
        inc rdi
        call parse_uint
        neg rax
        inc rdx
        ret
       
; Принимает указатель на строку, указатель на буфер и длину буфера 
;   rdi - ук. на строку
;   rsi - ук. на буфер
;   rdx - длина буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor r9, r9 ; для длины
    call string_length ; в rax длина строки
    mov r9, rax
    inc rax ; + знак нуля
    cmp rax, rdx 
    ja .too_long
    mov rcx, rax ; счетчик для loop 
    xor r8, r8 ; для записи
    .loop:
        test rcx, rcx
        jz .end
        mov r8b, byte [rdi] ; в r8b символ
        inc rdi
        mov byte [rsi], r8b ; в буфер символ из r8b
        inc rsi
        dec rcx
        jmp .loop
    .too_long:
        xor rax, rax
        ret 
    .end:
        mov rax, r9
        ret

